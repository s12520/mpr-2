import com.project1.jdbcdemo.domain.*;
import com.project1.jdbcdemo.service.*;


	public class App {

		public static void main(String[] args) {
			Person person = new Person();
			person.setName("Jan Kowalski");
			person.setYob(1990);
			PersonManager personManager = new PersonManager();
			personManager.addPerson(person);
			//System.out.println(person);
			
			
			Address address = new Address();
			address.setCity("Test");
			address.setCountry("Test");
			address.setStreet("Test");
			address.setPostalCode("Test");
			address.setHouseNumber("Test");
			address.setLocalNumber("Test");
			AddressManager addressManager = new AddressManager();
			addressManager.addAddress(address);
			
			Permission permission = new Permission();
			permission.setName("Test");
			PermissionManager permissionManager = new PermissionManager();
			permissionManager.addPermission(permission);
			
			User user = new User();
			user.setUsername("test1");
			user.setPassword("test2");
			UserManager userManager = new UserManager();
			userManager.addUser(user);
			
			Role role = new Role();
			role.setName("Warior");
			RoleManager roleManager = new RoleManager();
			roleManager.addRole(role);
			
		}

	}